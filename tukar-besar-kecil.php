<?php
    function tukar_besar_kecil($string){
        return $string ? (ctype_upper($string[0]) ? strtolower($string[0]) : strtoupper($string[0])).tukar_besar_kecil(substr($string,1)) : "";
    }
    
    // TEST CASES
    echo tukar_besar_kecil('Hello World')."\n"; // "hELLO wORLD
    echo tukar_besar_kecil('I aM aLAY')."\n"; // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!!')."\n"; // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me')."\n"; // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW')."\n"; // "001-a-3-5tRDyw"

?>