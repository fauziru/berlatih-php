<?php
    function ubah_huruf($string){
        return $string ? chr(ord($string[0]) + 1).ubah_huruf(substr($string,1)) : "";
    }
    
    // TEST CASES
    echo ubah_huruf('wow')."\n"; // xpx
    echo ubah_huruf('developer')."\n"; // efwfmpqfs
    echo ubah_huruf('laravel')."\n"; // mbsbwfm
    echo ubah_huruf('keren')."\n"; // lfsfo
    echo ubah_huruf('semangat')."\n"; // tfnbohbu

?>